FROM gliderlabs/alpine:3.2

RUN apk-install docker git

RUN apk-install go
ENV GOPATH /go
ENV PATH $GOPATH/bin:$PATH

RUN go get github.com/ddollar/init
RUN go get -u github.com/aws/aws-sdk-go/...

WORKDIR /go/src/github.com/usr/app
COPY . /go/src/github.com/usr/app
RUN go get .

ENV PORT 5001
ENTRYPOINT ["/go/bin/init"]
CMD ["app"]
