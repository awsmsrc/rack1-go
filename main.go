package main

import (
	"fmt"
	"html"
	"log"
	"net/http"
	"os"
)

func main() {
	// read config from environment
	port := os.Getenv("PORT")

	// start server
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "<h1>Hello, %q</h1>", html.EscapeString(r.URL.Path))
	})

	log.Println("Listening=" + port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
